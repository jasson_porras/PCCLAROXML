const csv = require('csv-parser');
const fs = require('fs');
const parseXML = require('./stringToXml')
let ClaroTC;

fs.createReadStream('TransactionsPCClaro.csv')
  .pipe(csv())
  .on('data', (row) => {
       ClaroTC = parseXML.parseXML(row.Request);

  })
  .on('end', () => {
    console.log(ClaroTC.length);
    console.log('CSV file successfully processed');
  });
