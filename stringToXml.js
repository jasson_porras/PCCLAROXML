let xml2js = require('xml2js');

let clartTCData  = {}
let claroTC = []

let parseXML = (xml) => {

  var parser = new xml2js.Parser({ explicitArray: false });

  parser.parseString(xml, function (err, result) {
    if (!err) {

      clartTCData.category = null,
      clartTCData.ced =  result['soapenv:Envelope']['soapenv:Body']['urn:ActivateOfferRequestMessage']['paymentMethod']['creditCard']['number'],
      clartTCData.code_offert=  null,
      clartTCData.date =  result['soapenv:Envelope']['soapenv:Body']['urn:ActivateOfferRequestMessage']['header']['externalTransactionDate'].toString().replace(/T/, ' '),
      clartTCData.email=  result['soapenv:Envelope']['soapenv:Body']['urn:ActivateOfferRequestMessage']['paymentMethod']['paymentReference2'],
      clartTCData.ip=  null,
      clartTCData.mediaId=  result['soapenv:Envelope']['soapenv:Body']['urn:ActivateOfferRequestMessage']['header']['mediaId'],
      clartTCData.name= result['soapenv:Envelope']['soapenv:Body']['urn:ActivateOfferRequestMessage']['paymentMethod']['creditCard']['name'],
      clartTCData.package_code= result['soapenv:Envelope']['soapenv:Body']['urn:ActivateOfferRequestMessage']['offerId'],
      clartTCData.package_subcode= result['soapenv:Envelope']['soapenv:Body']['urn:ActivateOfferRequestMessage']['offerId'],
      clartTCData.serviceNumber=  result['soapenv:Envelope']['soapenv:Body']['urn:ActivateOfferRequestMessage']['subscriberId'],
      clartTCData.status=  "SUCCES",
      clartTCData.status_description= "Su requerimiento fue receptado de forma exitosa",
      clartTCData.ticket=  "BUSCAR EN KUSHKI",
      clartTCData.token=  null,
      clartTCData.total=  null,
      clartTCData.trans= null,
      clartTCData.transId=  result['soapenv:Envelope']['soapenv:Body']['urn:ActivateOfferRequestMessage']['header']['externalTransactionId'],
      clartTCData.type=  result['soapenv:Envelope']['soapenv:Body']['urn:ActivateOfferRequestMessage']['paymentMethod']['paymentReference1'],
      clartTCData.typeTC= result['soapenv:Envelope']['soapenv:Body']['urn:ActivateOfferRequestMessage']['paymentMethod']['creditCard']['type'],
      clartTCData.ua= null,
      claroTC.push(clartTCData);
    }
  });

return claroTC;

}

module.exports = {parseXML}
